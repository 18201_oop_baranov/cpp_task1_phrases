//
// Created by пк on 25.09.2019.
//

#include <sstream>
#include "phrases.h"

ProgramArgs_t get_args(int argc, char *argv[])
{
    char const optstring[] = ":m:n:";
    std::stringstream os;

    ProgramArgs_t args;

    char * endptr;

    int opt;

    while ((opt = getopt(argc, argv, optstring)) != -1)
    {
        switch (opt)
        {
            case 'n':
                args.n = strtol(optarg, &endptr, 10);
                if (errno == ERANGE or *endptr != 0 or args.n < 0)
                {
                    os << "unable to convert \"" << optarg << "\" to unsigned integer" << std::endl;
                    throw std::runtime_error(os.str());
                }
                break;
            case 'm':
                args.m = strtol(optarg, &endptr, 10);
                if (errno == ERANGE or *endptr != 0 or args.m < 0)
                {
                    os << "unable to convert \"" << optarg << "\" to unsigned integer" << std::endl;
                    throw std::runtime_error(os.str());
                }
                break;
            case ':':
                os << "option \"" << char(optopt) << "\" is missing an argument" << std::endl;
                throw std::runtime_error(os.str());
            case '?':
            default:
                os << "unknown option \"" << char(optopt) << "\"" << std::endl;
                throw std::runtime_error(os.str());
        }
    }

    args.file_name = argv[optind];

    return args;
}

int get_phrases(int argc, char *argv[])
{
    std::string text;
    std::string current;



    ProgramArgs_t args;

    try
    {
        args = get_args(argc, argv);
    }
    catch (std::runtime_error & error)
    {
        std::cout << error.what() << std::endl;
    }

    int len = args.n;
    int min = args.m;
    char * filename = args.file_name;

    int i=0, k=0;

    std::map<std::string, int> book;

    std::vector<std::string> vect;

    std::ifstream in_file;

    if (filename and strcmp(filename, "-") != 0)
    {
        in_file.open(filename);

        if (not in_file.is_open())
        {
            std::cout << "Unable to open input file " << filename << std::endl;
            return 0;
        }
    }

    std::istream & in = in_file.is_open() ? in_file : std::cin;

    while (in >> text) {
        //if (text == "stop") break;
        k++;
        vect.push_back(text);
        if (k == len) {
            for (int j = 0; j < vect.size(); j++) {
                current += vect[j];
                if (j != vect.size() - 1) current += ' ';
            }
            book[current]++;
            //std::cout << book[current] << std::endl;
            current = "";
            vect.erase(vect.begin());
            k--;
        }
    }

    std::vector<std::pair<std::string,int>> pair_vect(book.size());

    auto it = std::copy_if (
            book.begin(),
            book.end(),
            pair_vect.begin(),
            [&min](std::pair<std::string,int> const & p1){ return p1.second >= min;} );

    pair_vect.resize(unsigned(std::distance(pair_vect.begin(), it)));  // shrink container to new size

    std::sort(
            pair_vect.begin(),
            pair_vect.end(),
            [](std::pair<std::string,int> const & p1, std::pair<std::string,int> const & p2)
            {
                return p1.second > p2.second;
            });

    for (auto const & pair : pair_vect)
    {
        if (pair.second>min-1)  std::cout << pair.first << " (" << pair.second << ")" << std::endl;
    }

    in_file.close();
    return 0;
}

