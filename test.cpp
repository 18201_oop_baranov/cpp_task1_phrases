#define CATCH_CONFIG_MAIN


#include "catch.hpp"
#include "phrases.h"


TEST_CASE("test argument parser")
{
    ProgramArgs_t args;

    SECTION("normal args")
    {
        optind = 1;
        char const * argv[] = {"random.exe", "-n", "3", "-m4", "../in.txt", nullptr};

        REQUIRE_NOTHROW(args = get_args(5, const_cast<char **>(argv)));

        REQUIRE(args.n == 3);
        REQUIRE(args.m == 4);
        REQUIRE(std::string(args.file_name) == "../in.txt");
    }

    SECTION("abnormal args")
    {
        SECTION("negative parameter value")
        {
            optind = 1;
            char const * argv[] = {"random.exe", "-n", "-1", "-m4", "../in.txt", nullptr};

            REQUIRE_THROWS(args = get_args(5, const_cast<char **>(argv)));
        }

        SECTION("overflow parameter value")
        {
            optind = 1;
            char const * argv[] = {"random.exe", "-n", "1", "-m999999999999999999999999999999999", "../in.txt", nullptr};

            REQUIRE_THROWS(args = get_args(5, const_cast<char **>(argv)));
        }

        SECTION("nan parameter value")
        {
            optind = 1;
            char const * argv[] = {"random.exe", "-n", "1", "-m abc", "../in.txt", nullptr};

            REQUIRE_THROWS(args = get_args(5, const_cast<char **>(argv)));
        }
    }
}
