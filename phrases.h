#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <getopt.h>
#include <algorithm>
#include <cstring>

struct ProgramArgs_t
{
    long n = 2;
    long m = 2;
    char * file_name = nullptr;
};

ProgramArgs_t get_args(int argc, char *argv[]);

int get_phrases(int argc, char *argv[]);
